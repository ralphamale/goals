require 'capybara/rspec'

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"
end

def sign_up(username)
  visit new_user_url
  fill_in "Username", with: username
  fill_in "Password", with: 'abcdefg'
  click_button 'Sign Up'
end

def sign_up_as_tommy
  sign_up("Tommy")
end

def sign_in_as_tommy
  visit new_session_url
  fill_in "Username", with: "Tommy"
  fill_in "Password", with: 'abcdefg'
  click_button 'Sign In'
end

def sign_in_as_god
  visit new_session_url
  fill_in "Username", with: "God"
  fill_in "Password", with: "God"
  click_button 'Sign In'
end

def log_out
  click_button "Sign Out"
end

def create_goal
  fill_in "New Goal", with: "Get laid"
  click_button "Create Goal"
end

def edit_goal
  fill_in "Goal", with: "Be a virgin"
  click_button "Update Goal"
end

def delete_goal
  click_button "Delete Goal"
end

def visit_tommy
  tommy = User.find_by_username("Tommy")
  visit user_url(tommy)
end