require 'spec_helper'

feature "the signup process" do

  it "has a new user page" do
    visit new_user_url
    expect(page).to have_content "Sign Up"
  end

  feature "signing up a user" do

    it "shows username on the homepage after signup" do
      sign_up_as_tommy
      expect(page).to have_content "Tommy"
    end
  end

end

feature "logging in" do

  before(:each) do
    sign_up_as_tommy
    log_out
  end

  it "shows username on the homepage after login" do
    sign_in_as_tommy
    expect(page).to have_content "Tommy"
  end

end

feature "logging out" do

  before(:each) do
    sign_up_as_tommy
    log_out
  end

  it "begins with logged out state" do
    visit new_session_url
    expect(page).to_not have_content "Sign Out"
  end

  it "doesn't show username on the homepage after logout" do
    sign_in_as_tommy
    log_out
    expect(page).not_to have_content "Tommy"
  end

end

feature "admins" do

  it "has admin privileges on other people's accounts" do
    admin = Admin.new(username: "God", password: "God")


    sign_up_as_tommy
    sign_in_as_tommy
    create_goal
    log_out
    admin.save!
    sign_in_as_god
    visit_tommy
    expect(page).to have_button("Update Goal")
    expect(page).to have_button("Delete Goal")

  end

end