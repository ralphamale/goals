require 'spec_helper'



feature 'Showing goals' do
  before(:each) do
    sign_up_as_tommy
    sign_in_as_tommy
  end

  it "has no goals" do
    expect(page).to have_content("No goals")
  end

  it "has goals when there is some" do
    create_goal
    expect(page).to_not have_content("No goals")
  end
end

feature 'Creating goals' do
  before(:each) do
    sign_up_as_tommy
    sign_in_as_tommy
  end

  it "makes a goal" do
    create_goal
    expect(page).to have_content("Get laid")
  end
end

feature 'Editing goals' do

  before(:each) do
    sign_up_as_tommy
    sign_in_as_tommy
  end

  it "changes goal name" do
    create_goal
    edit_goal
    expect(page).to have_content("Be a virgin")
    expect(page).to_not have_content("Get laid")
  end

  it "changes status of goal" do
    create_goal
    click_button("Complete Goal")
    expect(page).to have_content("Get laid: Completed")
  end
end

feature 'Deleting goals' do

  before(:each) do
    sign_up_as_tommy
    sign_in_as_tommy
  end

  it "removes goal" do
    create_goal
    delete_goal
    expect(page).to_not have_content("Get laid")
  end
end
