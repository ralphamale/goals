require 'spec_helper'

feature "it has admin privileges on other people's accounts" do
  let (:admin) { Admin.create(username: "God", password: "God") }

  sign_up_as_tommy
  sign_in_as_tommy
  create_goal
  log_out
  sign_in_as_god
  visit_tommy
  expect(page).to have_button("Update Goal")
  expect(page).to have_button("Delete Goal")

end