Goal::Application.routes.draw do
  resources :users, only: [:new, :create, :show] do
    resources :objectives, only: [:create, :update, :destroy]
  end
  resource :session, only: [:new, :create, :destroy]
end
