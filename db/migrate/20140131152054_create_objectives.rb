class CreateObjectives < ActiveRecord::Migration
  def change
    create_table :objectives do |t|
      t.string :body, null: false
      t.boolean :completed, default: false
      t.integer :user_id

      t.timestamps
    end

    add_index :objectives, :user_id

  end
end
