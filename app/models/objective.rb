class Objective < ActiveRecord::Base
  attr_accessible :body, :completed

  validates :body, presence: true

  belongs_to :user, inverse_of: :objectives

end
