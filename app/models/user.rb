class User < ActiveRecord::Base
  attr_reader :password
  attr_accessible :username, :password
  validates :password_digest, :session_token, :username, presence: true
  after_initialize :ensure_session_token

  has_many :objectives, inverse_of: :user

  def has_objectives?
    !self.objectives.empty?
  end

  def self.find_by_credentials(username, password)
    user = User.find_by_username(username)

    return nil unless !!user

    user.is_password?(password) ? user : nil
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def reset_session_token
    self.session_token = SecureRandom::urlsafe_base64(16)
    self.save!
  end

  def ensure_session_token
    self.session_token ||= SecureRandom::urlsafe_base64(16)
  end
end

class Admin < User

end