class AdminsController < ApplicationController

    def new

    end

    def create
      @admin = Admin.new(params[:admin])

      if @admin.save
        login_admin!(@admin)
        redirect_to @admin
      else
        render :new
      end
    end

    def show
      @admin = Admin.find(params[:id])
    end




end
