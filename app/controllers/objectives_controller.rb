class ObjectivesController < ApplicationController

  def create
    @user = User.find(params[:user_id])
    @user.objectives.new(params[:objective])

    if @user.save
      redirect_to @user
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to @user
    end
  end

  def update
    @objective = Objective.find(params[:id])

    if @objective.update_attributes(params[:objective])
      redirect_to @objective.user
    else
      flash[:errors] = @objective.errors.full_messages
      redirect_to @objective.user
    end
  end

  def destroy
    @objective = Objective.find(params[:id])
    @objective.destroy
    redirect_to @objective.user
  end

end
