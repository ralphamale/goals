class UsersController < ApplicationController

  def new

  end

  def create
    @user = User.new(params[:user])

    if @user.save
      login_user!(@user)
      redirect_to @user
    else
      render :new
    end
  end

  def show
    @user = User.find(params[:id])
  end

end
