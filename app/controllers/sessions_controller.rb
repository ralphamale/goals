class SessionsController < ApplicationController

  def new

  end

  def create
    @user = User.find_by_credentials(
    params[:user][:username],
    params[:user][:password]
    )

    if @user
      login_user!(@user)
      redirect_to user_url(@user)
    else
      flash.now[:errors] = "invalid username/password"
      render :new
    end
  end

  def destroy
    log_out
    render :new
  end
end
